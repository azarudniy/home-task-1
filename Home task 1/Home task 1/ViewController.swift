//
//  ViewController.swift
//  Home task 1
//
//  Created by Александр Зарудний on 9/8/21.
//

import SnapKit
import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
       
    lazy var logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "outline_done_black_48pt.png"))
        return imageView
    }()
    
    lazy var welcomeLabel: UILabel = {
        let label = UILabel()
        label.text = NSLocalizedString("title.welcoming", comment: "")
        label.font = label.font.withSize(20)
        return label
    }()
    
    lazy var pickerLanguage: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()
    
    private var arrayLocation = ["🇬🇧 \("picker.eng".localized("en"))", "🇧🇾 \("picker.blr".localized("en"))", "🇷🇺 \("picker.rus".localized("en"))"]
    
    
    lazy var buttonLightScheme: UIButton = {
        return getButton("button.light", "en")
    }()
    
    lazy var buttonDarkScheme: UIButton = {
        return getButton("button.dark", "en")
    }()
    
    lazy var buttonSystemScheme: UIButton = {
        return getButton("button.system", "en")
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setColorScheme(self.traitCollection.userInterfaceStyle)
        subViews()
    }
    
    private func getButton(_ title: String, _ locale: String) -> UIButton {
        let button = UIButton()
        button.setTitle(title.localized(locale), for: .normal)
        button.backgroundColor = .darkGray
        button.tintColor = .black
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        return button
    }
    
    @objc func buttonAction(sender: UIButton!) {
        switch sender {
        case buttonLightScheme:
            setColorScheme(.light)
            
        case buttonDarkScheme:
            setColorScheme(.dark)
            
        case buttonSystemScheme:
            setColorScheme(self.traitCollection.userInterfaceStyle)
            
        default:
            break
        }
        sender.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIView.AnimationOptions.allowUserInteraction,
                       animations: {sender.transform = CGAffineTransform.identity},
                       completion: { Void in()})
        sender.backgroundColor = .lightGray
        
        if buttonLightScheme != sender {
            buttonLightScheme.backgroundColor = .darkGray
        }
        
        if buttonDarkScheme != sender {
            buttonDarkScheme.backgroundColor = .darkGray
        }
        
        if buttonSystemScheme != sender {
            buttonSystemScheme.backgroundColor = .darkGray
        }
        
    }
    
    private func setColorScheme(_ scheme: UIUserInterfaceStyle) {
        switch scheme {
        case .light:
            view.backgroundColor = .white
            welcomeLabel.textColor = .black
            pickerLanguage.setValue(UIColor.black, forKey: "textColor")
            
        case .dark:
            view.backgroundColor = .black
            welcomeLabel.textColor = .white
            pickerLanguage.setValue(UIColor.white, forKey: "textColor")
            
        default:
            break
        }
    }
    
    private func subViews() {
        view.addSubview(logoImageView)
        view.addSubview(welcomeLabel)
        view.addSubview(pickerLanguage)
        view.addSubview(buttonLightScheme)
        view.addSubview(buttonDarkScheme)
        view.addSubview(buttonSystemScheme)
        pickerLanguage.delegate = self
        pickerLanguage.dataSource = self
        
        logoImageView.snp.makeConstraints {
            $0.top.equalToSuperview().inset(50)
            $0.centerX.equalToSuperview()
        }
        
        welcomeLabel.snp.makeConstraints {
            $0.top.equalTo(logoImageView.snp.bottom).inset(-20)
            $0.centerX.equalToSuperview()
        }
        
        pickerLanguage.snp.makeConstraints {
            $0.width.equalToSuperview()
            $0.height.equalTo(100)
            $0.top.equalTo(welcomeLabel.snp.bottom).inset(-20)
            $0.centerX.equalToSuperview()
        }
        
        buttonLightScheme.snp.makeConstraints{
            $0.width.equalTo(80)
            $0.leading.equalToSuperview().inset(20)
            $0.top.equalTo(pickerLanguage.snp.bottom).offset(20)
        }
        
        buttonDarkScheme.snp.makeConstraints{
            $0.centerX.equalToSuperview()
            $0.width.equalTo(80)
            $0.top.equalTo(pickerLanguage.snp.bottom).offset(20)
        }
        
        buttonSystemScheme.snp.makeConstraints{
            $0.width.equalTo(80)
            $0.trailing.equalToSuperview().inset(20)
            $0.top.equalTo(pickerLanguage.snp.bottom).offset(20)
        }

    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayLocation.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayLocation[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
        case 0:
            setLocale("en")
        case 1:
            setLocale("be-BY")
        case 2:
            setLocale("ru")
        default:
            break
        }
        pickerLanguage.reloadAllComponents()
    }
    
    private func setLocale(_ lang: String){
        welcomeLabel.text = "title.welcoming".localized(lang)
        arrayLocation = ["🇬🇧 \("picker.eng".localized(lang))", "🇧🇾 \("picker.blr".localized(lang))", "🇷🇺 \("picker.rus".localized(lang))"]
        buttonLightScheme.setTitle("button.light".localized(lang), for: .normal)
        buttonDarkScheme.setTitle("button.dark".localized(lang), for: .normal)
        buttonSystemScheme.setTitle("button.system".localized(lang), for: .normal)
    }
}

extension String {
    func localized(_ lang: String) -> String {
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

